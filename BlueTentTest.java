import java.util.Arrays;

public class BlueTentTest {

    /**
     * This function outputs the numbers from 1 to 100.
     * For multiples of three it outputs “Fizz” instead of the number,
     * and for the multiples of five it outputs “Buzz”.
     * For numbers which are multiples of both three and five, it outputs “FizzBuzz”.
     *
     */
    public void printNumbers(){
        for(int i=1;i<=100;i++){
            if(i%15==0){
                print("FizzBuzz");
            }else if(i%5==0){
                print("Buzz");
            }else if(i%3==0){
                print("Fizz");
            }else{
                print(i+"");
            }
        }
    }

    /**
     * This function returns array of tax amount in cents
     *
     * @param amount Base amount
     * @param taxPercent Tax percentage to be applied in base amount
     * @return arr Array of taxamount in cents
     */
    public double[] taxInCents(double amount, double taxPercent){

        double taxAmount = amount * taxPercent / 100.0;
        return new double[]{taxAmount*100};
    }


    /**
     * This function returns array of tax amount in cents
     *
     * @param string1 first string
     * @param string2 second string
     * @return boolean true or false based on whether strings are anagram or not
     */
    public boolean anagramTest(String string1, String string2) {

        //remove whitespaces and convert all character into lowercase
        string1 = string1.replaceAll("\\s", "").toLowerCase();
        string2 = string2.replaceAll("\\s", "").toLowerCase();

        //check if the string length are equal
        if (string1.length() == string1.length()) {


            //To make use of sort function in java, split the string into char array
            char[] arrString1 = string1.toCharArray();
            char[] arrString2 = string2.toCharArray();

            //Sorting both arrString1 and arrString2
            Arrays.sort(arrString1);
            Arrays.sort(arrString2);


            //check if two array are equal
            if(Arrays.equals(arrString1, arrString2)){
                return true;
            }
        }
        return false;

    }

    /**
     * This function prints a string
     * @param s string to be printed
     */
    public static void print(String s){
        System.out.println(s);
    }

    public static void main(String[] args) {
        BlueTentTest btt = new BlueTentTest();

        print ("******************* Qn 1 *****************");
        //solution to question 1
        btt.printNumbers();


        print ("******************* Qn 2 *****************");
        //solution to question 2
        double[] taxAmountInCents = btt.taxInCents(10,8);
        for(double amt:taxAmountInCents){
            print("Tax amount in cents = "+amt);
        }

        print ("******************* Qn 3 *****************");
        //solution to question 3
        boolean isAnagram = btt.anagramTest("hello", "eh olld");
        print(isAnagram?"Given string are anagram":"Given string are not anagram");
    }
}
