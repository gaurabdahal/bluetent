# Test

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Running](#running)

## Introduction

This test class solves three basic problems. There are following three functions   

1. Function that outputs the numbers from 1 to 100. For multiples of three output “Fizz” instead of the number, and for the multiples of five output “Buzz”. For numbers which are multiples of both three and five output “FizzBuzz”.     
2. Function that takes two arguments; an amount, and a tax percentage. Return an array with the amount of tax in cents.      
3. Function that takes two words as its arguments and returns true if they are anagrams (contain the exact same letters) and false otherwise.       

### Prerequisites

It is developed and compiled against java 1.8 in Mac OS 10, so you need JDK 1.8 to compile and run this program.

## Running

Compile this program by running    
`$ javac BlueTentTest.java`

and run the program     
`$ java BlueTentTest`

